<big>Manjaro 21.3</big>

Since we released Qonos end of last year all our developer teams worked hard to get the next release of Manjaro out there. We call it Ruah.

This release features the final release of Calamares v3.2. Partition module gained more support for LUKS partitions. Users module now has lists of forbidden login- and host-names, to avoid settings that will mess up the install.

The <big>GNOME edition</big> has received a major update to Gnome 42. A new global dark UI style preference has arrived. This can be used to request that apps use a dark UI style instead of light. The new setting can be found in the new Appearance panel in the Settings app, and is followed by most GNOME apps.

GTK 4 and libadwaita provide next generation capabilities for GNOME applications, and many GNOME apps have started to use these components for GNOME 42. As a result, these apps have better performance, a new modern UI style, and new user interface elements.

GNOME 42 includes a valuable set of performance improvements, as the GNOME community continues its work to enhance system speed and resource usage.

The <big>Plasma edition</big> comes with the latest Plasma 5.24 series. Plasma is all about customization, and we have worked on making the process clear and simple so you can adapt your environment exactly to how you like it.

Desktop Panels are now easier to move around and stick to any edge you want, as you can now drag them from anywhere on their toolbar while in Edit Mode. You can better leverage the power of multiple desktops in Plasma 5.24 with the new Overview effect, which is much similar to how GNOME Overview works. This allows you to get a view of different workspaces, interact with all the various windows and applications you have open in the background and the ability to search your system too. 

Now it’s easy to become a KRunner whiz: click on the question mark icon on the toolbar and KRunner will show a list of available plugins and how to use them. Click on a plugin from the list and you’ll see all the information you need to use it to its fullest.

With our <big>XFCE edition</big>, we have now Xfce 4.16. The window manager received lots of updates and improvements again in the area of compositing and GLX. Support for fractional scaling was added to the display dialog, along with highlighting the preferred mode of a display with an asterisk and adding aspect ratios next to resolutions. The settings manager has improved search and filter capabilities. Thunar file manager received a boatload of fixes and quite a few notable features, including pause for copy/move operations, support for queued file transfer, remembering view settings per directory and support for transparency in Gtk themes.

Kernel 5.15 LTS is used for this release, such as the latest drivers available to date. With 5.4 LTS and 5.10 LTS we offer additional support for older hardware as needed.

We hope you enjoy this release and let us know what you think of Ruah.
